package com.spark.share

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-13 16:38:26
  */
object SparkBroadCast {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))
    // 使用广播变量
    val b_cast: Broadcast[Seq[String]] = context.broadcast(Seq("arr", "scala", "hive"))
    // 添加过滤操作
    // 过滤操作
    rdd.filter(b_cast.value.contains(_)).foreach(println)
    context.stop()

  }
}
