package com.spark.share

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-13 17:22:09
  */
object SparkAggregateSimple {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val rdd: RDD[Int] = context.makeRDD(1.to(50))
    val sum: LongAccumulator = context.longAccumulator("acc")
    rdd.map(data=>{

      sum.add(data)
    }).collect()
    println(sum.value)
    context.stop()
  }
}
