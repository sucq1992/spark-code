package com.spark.utils

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory}
import org.apache.hadoop.hbase.{HBaseConfiguration, HConstants}

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-11 10:48:13
  */
object HbaseUtils {

  def getConf(): Configuration = {
    val configuration: Configuration = HBaseConfiguration.create()
    configuration.set(HConstants.CLIENT_ZOOKEEPER_QUORUM, "node")
    configuration.set(HConstants.CLIENT_ZOOKEEPER_CLIENT_PORT, "2181")
    configuration
  }

  def getConnection(configuration: Configuration): Connection = {
    val connection: Connection = ConnectionFactory.createConnection(configuration)
    connection
  }

  def closeConn(connection: Connection) = {
    connection.close()
  }

}
