package com.spark.utils

import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-11 10:36:14
  */
object SparkUtils{

  def getSession: SparkSession = {
    val session: SparkSession = SparkSession.builder().appName(this.getClass.getName)
      .master("local[*]").getOrCreate()
    session
  }

  def getContext: SparkContext = {
    val conf: SparkConf = new SparkConf().setMaster("local[*]")
      .setAppName(this.getClass.getName)
    //    .set("spark.driver.allowMultipleContexts","true")
    val context: SparkContext = new SparkContext(conf)
    context
  }

  def getStreamingContext: StreamingContext = {
    val conf: SparkConf = new SparkConf().setMaster("local[*]")
      .setAppName(this.getClass.getName)
    val streamingContext = new StreamingContext(conf, Seconds(30))
    streamingContext
  }


}

