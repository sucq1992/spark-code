package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-11 13:48:35
  */
object ActionOperator {
  def main(args: Array[String]): Unit = {
    /**
      * 代码中的action算子数量
      * 其实就是application 的job的数量
      * sc.runJob()
      *
      *
      */

    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))
    val data: Long = countOperator(rdd)
    println(data)

    val take_res: Array[String] = takeOperator(rdd)
    println(take_res.mkString("-"))

    val first_res: String = firstOperator(rdd)
    println(first_res)

    val rdd_res: RDD[String] = sampleOperator(rdd)
    rdd_res.foreach(println)

    val collect_res: Array[String] = collectOperator(rdd)
    collect_res.foreach(println)

    context.stop()
  }
  def countOperator(rdd:RDD[String]):Long={
    val res: Long = rdd.count()
    // Return the number of elements in the RDD.
    res
  }

  def takeOperator(rdd:RDD[String]) ={
    val strings: Array[String] = rdd.take(2)
    strings
  }
  def firstOperator(rdd:RDD[String])={
    val str: String = rdd.first()   // take(1) 调用take(1)
    str
  }

  def sampleOperator(rdd:RDD[String])={
    val rdd_res: RDD[String] = rdd.sample(true,0.54)
    rdd_res
  }

  def collectOperator(rdd:RDD[String])={
    val strings: Array[String] = rdd.collect()   //  Array.concat(results: _*)
    // Return an array that contains all of the elements in this RDD.
    // all the data is loaded into the driver's memory. 注意内存溢出  OOM  所有结果会加载到driver内存中
    // action算子可以在driver端取到  考虑数据量会不会太大
    strings

  }



}
