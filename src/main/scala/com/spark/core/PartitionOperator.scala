package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-13 09:25:49
  */
object PartitionOperator {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))

    repartitionOperator(rdd).foreach(println)

    coalesceOperator(rdd).foreach(println)

    context.stop()
  }

  /**
    * 充分区算子
    *  coalesce(numPartitions, shuffle = true)
    * @param rdd
    * @return
    */
  def repartitionOperator(rdd:RDD[String])={
    rdd.repartition(4)
  }

  /**
    * 缩减分区算子
    * def coalesce(numPartitions: Int, shuffle: Boolean = false,
    * @param rdd
    * @return
    */
  def coalesceOperator(rdd:RDD[String])={
    rdd.coalesce(2)
  }
}
