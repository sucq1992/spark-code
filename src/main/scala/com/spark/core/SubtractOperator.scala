package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-12 13:49:57
  */
object SubtractOperator {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    val text_res: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt",2)
    val result_1: RDD[(String, Int)] = text_res.flatMap(_.split(" ")).filter(_.nonEmpty)
      .map((_, 1))

    val result_2: RDD[(String, Int)] = context.makeRDD(Seq("git","a","hello"),1).map((_,1))

    val result: RDD[(String, Int)] = result_1.subtract(result_2)

    println(result.partitions.length)
    context.stop()

    /**
      *差集的并行度就是看前后顺序，
      * 并行度是父RDD的最大的那个
      */
  }
}
