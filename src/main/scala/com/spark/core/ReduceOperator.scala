package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-13 10:18:25
  */
object ReduceOperator {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val i: Int = context.makeRDD(1.to(12)).reduce((t1, t2) => {
      t1 + t2
    })
    println(i)
    context.stop()
  }
}
