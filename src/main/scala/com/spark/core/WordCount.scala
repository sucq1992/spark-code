package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-11 10:34:11
  */
object WordCount {
  def main(args: Array[String]): Unit = {
   val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_, 1))
      .reduceByKey(_ + _)
      .foreach(println)
    context.stop()
  }
}
