package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-12 14:16:50
  */
object CoGroupOperator {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    val text_res: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt",2)
    val result_1: RDD[(String, Int)] = text_res.flatMap(_.split(" ")).filter(_.nonEmpty)
      .map((_, 1))

    val result_2: RDD[(String, Int)] = context.makeRDD(Seq("git","a","hello"),1).map((_,1))

    /**
      * cogroup 关键的是key，
      */
    val data: RDD[(String, (Iterable[Int], Iterable[Int]))] = result_1.cogroup(result_2)

    context.stop()
  }
}
