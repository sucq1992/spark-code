package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-11 14:58:55
  */
object CheckPointOperator {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))
    /**
      * check point 为了容错
      * 切断了依赖关系
      * 保存在磁盘
      *
      * job 执行完成，从final rdd 向前追溯 找到ck
      * 重新开启一个新的job 从hdfs 的ck继续
      *
      * 需要上下文来设置保存路径
      * 如下：结合cache 和 persist来使用
      *
      * It is strongly recommended that this RDD is persisted in
      * * memory, otherwise saving it on a file will require recomputation.
      */
    context.setCheckpointDir("hdfs://xxxxx")  //must be HDFS path if running in cluster
    rdd.checkpoint()  //是一个新的job

    context.stop()
  }
}
