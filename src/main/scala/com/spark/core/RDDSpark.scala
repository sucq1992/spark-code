package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-11 13:34:05
  */
object RDDSpark {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))

    /**
      * Internally, each RDD is characterized by five main properties:
      *  - A list of partitions   分区
      *  - A function for computing each split  计算向数据移动
      *  - A list of dependencies on other RDDs   依赖
      *  - Optionally, a Partitioner for key-value RDDs (e.g. to say that the RDD is hash-partitioned)
      *  - Optionally, a list of preferred locations to compute each split on (e.g. block locations for
      * an HDFS file)
      *
      *
      * RDD的特性如上
      *
      */
    context.stop()
  }
}
