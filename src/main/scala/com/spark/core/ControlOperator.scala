package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-11 14:06:19
  */
object ControlOperator {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))

    /**
      * 持久化算子
      *   持久化的单位是partition
      *
      *   application 有多个job ，job有多个task
      *   task个数就是并行度，就是分区数
      */
      persistOperator(rdd)

    /**
      * 如果计算数据，那会在第第一个cache的时候，时间会变长
      * 但是第二次执行的时候，时间明显变短。 数据都在memory中
      * job 内部是并行的，job的外部是串行的
      * 有效的提高效率
      */

    context.stop()
  }

  /**
    * 可以选择是在内存或者磁盘，或者内存和磁盘
    * @param rdd
    * @return
    */
  def persistOperator(rdd:RDD[String])={
    val res: RDD[String] = rdd.persist(StorageLevel.DISK_ONLY)
  }

  /**
    * catch 调用persist 只保存在内存中
    * @param rdd
    * @return
    */
  def cacheOperator(rdd:RDD[String])={
    val rdd_res: RDD[String] = rdd.cache()
  }


}
