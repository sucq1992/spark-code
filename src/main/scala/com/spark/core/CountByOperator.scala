package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-13 09:46:50
  */
object CountByOperator {
  def main(args: Array[String]): Unit = {
    val context: SparkContext = SparkUtils.getContext
    context.setLogLevel("WARN")
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))
    val stringToLong: collection.Map[String, Long] = rdd.map((_,1)).countByKey()

    /**
      * 算子不必是键值对RDD
      */
    val tupleToLong: collection.Map[(String, Int), Long] = rdd.map((_,1)).countByValue()

    /**
      * 算子可以直接计算
      * countByValue调用的就是：countByKey() 如下操作
        map(value => (value, null)).countByKey()
      */
    val by_value: collection.Map[String, Long] = rdd.countByValue()

    context.stop()
  }
}
