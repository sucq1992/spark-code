package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-12 13:56:03
  */
object ForeachOrForeachPartition {
  def main(args: Array[String]): Unit = {
    /**
      * 如果foreach 相当于每条建立一个连接
      * 效率低
      *
      * foreachPartition
      *  按照分区建立连接，效率高，但是会造成OOM
      *
      *  map每次处理一条数据
      *
      *  map partition 每次处理一个分区
      *
      *
      */

    val context: SparkContext = SparkUtils.getContext
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt")
      .flatMap(_.split(" "))

    rdd.mapPartitions(data=>{
      data.map(res=>{
        res.concat("olo")
      })
    }).foreach(println)


    val distinct_res: RDD[String] = rdd.distinct()
    println(distinct_res.partitions.length)









    context.stop()
  }
}
