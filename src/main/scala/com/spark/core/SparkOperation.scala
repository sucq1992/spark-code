package com.spark.core

import com.spark.utils.SparkUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * @program: spark-code
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-11 16:22:42
  */
object SparkOperation {
  def main(args: Array[String]): Unit = {
    /**
      * standalone
      * yarn
      *
      * action   触发算子 触发job的算子
      * transform   转换算子
      * 持久化算子  cache persist
      * check_point 追溯 mark标记 重新计算
      * job 完成后得到final_rdd 需要设置set.dir()
      * 持久化到hdfs ，可搭配persist来优化
      *
      */

    /**
      * 补充算子：
      *   join 算子在操作的时候，得到的结果的并行度取决于左边的并行度最大的那一个算子
      *   根据父RDD拿到最大的并行度，就是自己的并行度
      *
      * union的并行度是父RDD的并行度之和
      *
      */
    val context: SparkContext = SparkUtils.getContext
//    context.setLogLevel("WARN")
    val rdd: RDD[String] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt",3)
      .flatMap(_.split(" "))
    val res: RDD[(String, Int)] = rdd.map((_,1))

    val res2: RDD[(String, Int)] = context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word2.txt",2)
      .flatMap(_.split(" ")).map((_, 1))

    val res_join: RDD[(String, (Int, Int))] = res.join(res2)
    println(res_join.partitions.length)
    res_join.foreach(println)

    val res_left: RDD[(String, (Int, Option[Int]))] = res.leftOuterJoin(res2)
    res_left.foreach(println)
    println(res_left.partitions.length)

    val right_join: RDD[(String, (Option[Int], Int))] = res.rightOuterJoin(res2)
    right_join.foreach(println)
    println(right_join.partitions.length)

    val union_res: RDD[(String, Int)] = res.union(res2)
    println(union_res.partitions.length)
    context.stop()
  }
}
