package com.spark.code;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author run
 * @program: spark-code
 * @Version run
 * @Modified run
 * @date 2021-08-11 15:09:01
 */
public class SparkDemo {
  public static void main(String[] args) {
    //
    SparkConf conf = new SparkConf();
    conf.setMaster("local[*]");
    conf.setAppName("java wc");
    JavaSparkContext context = new JavaSparkContext(conf);
    context.setLogLevel("WARN");
    JavaRDD<String> javaRDD =
        context.textFile("/Users/run/Downloads/workspaceIDEA/spark-code/data/word.txt");
    JavaRDD<String> stringJavaRDD =
        javaRDD.flatMap(
            new FlatMapFunction<String, String>() {

              public Iterator<String> call(String s) throws Exception {
                String[] s1 = s.split(" ");
                List<String> list = Arrays.asList(s1);
                return list.iterator();
              }
            });

    JavaPairRDD<String, Integer> stringIntegerJavaPairRDD =
        stringJavaRDD.mapToPair(
            new PairFunction<String, String, Integer>() {
              public Tuple2<String, Integer> call(String word) throws Exception {
                return new Tuple2(word, 1);
              }
            });

    JavaPairRDD<String, Integer> integerJavaPairRDD =
        stringIntegerJavaPairRDD.reduceByKey(
            new Function2<Integer, Integer, Integer>() {
              public Integer call(Integer v1, Integer v2) throws Exception {
                return v1 + v2;
              }
            });

    integerJavaPairRDD.foreach(
        new VoidFunction<Tuple2<String, Integer>>() {
          public void call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
            System.out.println(stringIntegerTuple2);
          }
        });

    context.stop();
  }
}
